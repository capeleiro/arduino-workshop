Workshop de Arduino e Introducao a Robotica
====
Este repositorio contem toda a informacao necessaria para o acompanhamento do workshop. Poderas encontrar:

* Pre-requisitos: lista de software necessario, com links e instrucoes [/Requisitos];
* Apresentacao em PowerPoint [/Apresentacao];
* Ficheiros relativos a cada uma das fases do workshop para uma mais facil compreensao [/Exercicios/#nn]: 
    * Codigo que ira ser criado [/Exercicios/#nn/Codigo]; 
    * Esquemas electronicos usados [/Exercicios/#nn/Esquemas]; 
    * Documentos e links que contem informacao relevante [/Exercicios/#nn/Info];