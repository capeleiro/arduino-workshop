
#include "TB6612FNG.h"

#define left_PWM_pin   3
#define left_pin1      9
#define left_pin2      8

#define right_PWM_pin  5
#define right_pin1     11
#define right_pin2     12

Motor *left;
Motor *right;

// the setup routine runs once when you press reset:
void setup() {   
  
  left = new Motor(left_PWM_pin, left_pin1, left_pin2);
  right = new Motor(right_PWM_pin, right_pin1, right_pin2);
  
}

// the loop routine runs over and over again forever:
void loop() {
  
  left->ChangeSpeed(125);
  right->ChangeSpeed(125);
  
  left->ChangeDirection(FORWARD);
  right->ChangeDirection(FORWARD);
    
  left->Rotate();
  right->Rotate();
  delay(500);
    
  left->Stop();
  right->Stop();
  delay(250);
  
  left->ChangeDirection(REVERSE);
  right->ChangeDirection(REVERSE);
    
  left->Rotate();
  right->Rotate();
  delay(500);  
}
