
#include "TB6612FNG.h"
#include "NewPing.h"

#define left_PWM_pin   3
#define left_pin1      9
#define left_pin2      8

#define right_PWM_pin  5
#define right_pin1     11
#define right_pin2     12

#define TRIGGER_PIN   7
#define ECHO_PIN      6
#define MAX_DISTANCE  150
#define THRESHOLD     20

Motor *left;
Motor *right;

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

// the setup routine runs once when you press reset:
void setup() {   
  
  left = new Motor(left_PWM_pin, left_pin1, left_pin2);
  right = new Motor(right_PWM_pin, right_pin1, right_pin2);
  
  left->ChangeSpeed(100);
  right->ChangeSpeed(100);  
}

// the loop routine runs over and over again forever:
void loop() {
  
  int ping, distance;
  
  /* Do 5 pings, discard out of range pings and return median in microseconds  */
  ping = sonar.ping_median(5);
  
  /* Convert the reflection time to cm */
  distance = sonar.convert_cm(ping);
  
  if(distance <= 20 && distance != 0){
      turnAround();
  }
  
  moveForward();  
}

void moveForward(){    
  left->ChangeDirection(FORWARD);
  right->ChangeDirection(FORWARD);    
  
  left->Rotate();
  right->Rotate();  
}

void turnAround(){  
  left->Stop();    
  right->Stop(); 
  delay(250);
  
  left->ChangeDirection(REVERSE);
  right->ChangeDirection(REVERSE);    
  
  left->Rotate();
  right->Rotate();  
  delay(350);
  
  right->Stop(); 
  delay(200);
  
  left->Stop();  
}
