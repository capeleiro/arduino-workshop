
#define  LED_PIN      13
#define  SENSOR_PIN   A0

#define  THRESHOLD    250

// the setup routine runs once when you press reset:
void setup() {    
  
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(9600);
  
}

// the loop routine runs over and over again forever:
void loop() {
  
  int ambient, reflected, real_value;
  
  /* LED is off - reading ambient noise */
  digitalWrite(LED_PIN, LOW);
  ambient = analogRead(SENSOR_PIN);
  
  delay(1);
  
  /* LED is on - reading the reflected light (plus ambient noise) */
  digitalWrite(LED_PIN, HIGH);
  reflected = analogRead(SENSOR_PIN);
  
  /* Estimating the real value without ambient noise */
  real_value = ambient - reflected;
  /* error catching */
  if(real_value < 0){
    real_value = -1;
  }
  
  Serial.print("Value read: ");
  Serial.println(real_value);
  
  /* Trying to infer whether the color is black or white */
  if(real_value > THRESHOLD){
    Serial.println("BRIGHT!");
    Serial.println(); /* empty line */
  }
  else if (real_value != -1){
    Serial.println("DARK!");
    Serial.println(); /* empty line */
  }
  else{
    Serial.println("error!");
    Serial.println(); /* empty line */
  }
  
  /* thi way the IDE input buffer is not overloaded */
  delay(250);
}
